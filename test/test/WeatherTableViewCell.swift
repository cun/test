
//
//  WeatherTableViewCell.swift
//  test
//
//  Created by john on 3/3/16.
//  Copyright © 2016 john. All rights reserved.
//

import UIKit
import SwiftyJSON

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel : UILabel!
    @IBOutlet weak var summaryLabel : UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setWeathData(data : JSON)
    {
        var text = ""
        for (key, value) in data
        {
            if key == "time"
            {
                timeLabel.text = getTime(value.number)
                continue;
            }
            
            
            text += key
            text += " : "
            
            if let string = value.string
            {
                text += string
            }
            else if let number = value.number
            {
                let keyString = key.lowercaseString
                if keyString.rangeOfString("time") != nil
                {
                    text += getTime(number)
                }
                else
                {
                    text += String(format:"%@", number)
                }
            }
            
            text += "\r\n"
        }
        summaryLabel.text = text
        

    }
    
    func getTime(number :  NSNumber? ) -> String
    {
        if number == nil
        {
            return ""
        }
        let date = NSDate(timeIntervalSince1970: number!.doubleValue)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        return dateFormatter.stringFromDate(date)
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



