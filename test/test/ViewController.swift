//
//  ViewController.swift
//  test
//
//  Created by john on 3/3/16.
//  Copyright © 2016 john. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper


class ViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    
    let weatchCellName = "WeatherTableViewCell"
    
    var dataJSON : JSON? = nil
    
    var hourlyArray = [JSON]()
    var dailyArray = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.rowHeight = UITableViewAutomaticDimension;
        tableView.estimatedRowHeight = 160.0
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }

    
    func getData()
    {
        if let url = NSURL(string: "https://api.forecast.io/forecast/1d7edfcd1c2193afc513bc9cb120491b/-33.8674769,151.20697759999996")
        {
            Alamofire.request(.GET, url , parameters: [ "":"" ])
                .responseJSON { response in
                    if let json = response.result.value {
                        let result = JSON(json)
                        self.dataJSON = result
                        
                        if let hourlyJsonArray = result["hourly"]["data"].array
                        {
                            for data in hourlyJsonArray
                            {
                                self.hourlyArray.append(data)
                            }
                        }
                        
                        if let dailyJsonArray = result["daily"]["data"].array
                        {
                            for data in dailyJsonArray
                            {
                                self.dailyArray.append(data)
                            }
                        }
                        
                        self.tableView.reloadData()
                    }
            }
            
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController : UITableViewDataSource
{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            return hourlyArray.count
        }
        else if section == 2
        {
            return dailyArray.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(weatchCellName, forIndexPath: indexPath) as! WeatherTableViewCell
        
        if indexPath.section == 0 && dataJSON != nil
        {
            cell.setWeathData(dataJSON!["currently"])
        }
        else if indexPath.section == 1 && hourlyArray.count > indexPath.row
        {
            cell.setWeathData(hourlyArray[indexPath.row])
        }
        else if dailyArray.count > indexPath.row
        {
            cell.setWeathData(dailyArray[indexPath.row])
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if dataJSON == nil
        {
            return nil
        }
        
        if section == 1
        {
            return dataJSON!["daily"]["summary"].string
        }
        else if section == 2
        {
            return dataJSON!["hourly"]["summary"].string
        }
        else
        {
            return dataJSON!["timezone"].string
        }
    }
    

}



